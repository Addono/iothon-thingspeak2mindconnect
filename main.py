import datetime
import json
import random
import sys

import thingspeak
import yaml
from mindconnectiot import MindConnectIot
from apscheduler.schedulers.background import BackgroundScheduler

from logic import quadro


latitude = 51.2433101
longitude = 6.7303924

def main():
    with open("config.yaml", 'r') as ymlfile:
        config = yaml.load(ymlfile, Loader=yaml.FullLoader)

    channel_id = config['thingspeak']['channel_id']
    channel = thingspeak.Channel(channel_id)
    mindconnect = MindConnectIot("TS_%s" % str(channel_id), config['mindconnect']['region'],
                                 config['mindconnect']['tenant_name'], config['mindconnect']['mcio_extension_username'],
                                 config['mindconnect']['password'])

    scheduler = BackgroundScheduler()
    scheduler.add_job(update, 'interval', seconds=10,
                      kwargs={'thingspeak_channel': channel, 'mindconnect': mindconnect, 'mappings': config['mappings']})
    scheduler.start()

    print("Mirroring Thingspeak Channel %s\n" % channel_id)
    print("PRESS ENTER TO ABORT\n")
    update(thingspeak_channel=channel, mindconnect=mindconnect, mappings=config['mappings'])

    # Wait until enter is pressed
    sys.stdin.readline()

    print("Aborting")
    scheduler.shutdown()


def update(thingspeak_channel: thingspeak.Channel, mindconnect: MindConnectIot, mappings: list):
    global latitude, longitude

    raw_response = thingspeak_channel.get({'results': 1})
    response = json.loads(raw_response)

    if len(response['feeds']) == 0:
        print('Nothing to update as the ThingSpeak channel is empty')
        return

    result = response['feeds'][0]

    def send_measurement_by_mapping(mapping: dict, value):
        mindconnect.sendMeasurement(mapping['fragment'], mapping['series'], value, mapping['unit'], result['created_at'])

    def send_alarm(severity: str, alarm_type: str, message: str = None):
        """
        :param severity: The severity of the alarm.
        :param alarm_type: The type of the alarm. 
        :type message: Optional additional message, only available if the severity is critical, major, minor or warning.
        """
        if severity == 'critical':
            mindconnect.sendCriticalAlarm(alarm_type, message)
        elif severity == 'major':
            mindconnect.sendMajorAlarm(alarm_type, message)
        elif severity == 'minor':
            mindconnect.sendMinorAlarm(alarm_type, message)
        elif severity == 'warning':
            mindconnect.sendWarningAlarm(alarm_type, message)
        else:
            mindconnect.sendAlarm(alarm_type, severity)

    def check_danger_zone(alarm: dict, mapping: dict, value: float):
        min = alarm['min'] if 'min' in alarm else float('-inf')
        max = alarm['max'] if 'max' in alarm else float('inf')
        severity = alarm['severity'] if 'severity' in alarm else 'warning'

        if min < value < max:
            message = alarm['message'] if 'message' in alarm else '{0} ({1} {2}) is in danger zone {3} {2}'
            send_alarm(severity, 'Exceeding safety threshold (%s) (%s %s)' % (severity, value, mapping['unit']), message.format(mapping['fragment'], value, mapping['unit'], min))

    def check_alarms(mapping: dict, value):
        if 'alarms' not in mapping:
            return

        for alarm in mapping['alarms']:
            if 'type' in alarm and alarm['type'] == 'dangerZone':
                check_danger_zone(alarm, mapping, value)

    for mapping in mappings:
        if mapping['type'] == 'oneToOne':
            value = result['field%s' % mapping['field']]
            send_measurement_by_mapping(mapping, value)
            check_alarms(mapping, value)
        elif mapping['type'] == 'quadro' or mapping['type'] == 'quadroInverted':
            input_values = [result['field%s' % field] for field in mapping['fields']]
            value = round(100 * quadro(input_values, mapping['optimal'], mapping['normals']), 2)

            if mapping['type'] == 'quadroInverted':
                value = 100 - value

            send_measurement_by_mapping(mapping, value)
            check_alarms(mapping, value)

    latitude += random.uniform(-0.0005, 0.005)
    longitude += random.uniform(-0.005, 0.0005)
    mindconnect.updateLocation(latitude, longitude, 0, 0, inventoryUpdate=True)

    print("Updated at "+str(datetime.datetime.now()))


if __name__ == "__main__":
    main()
