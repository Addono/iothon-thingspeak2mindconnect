import math


def quadro(c: list, d: list, n: list):
    """
    :param c: The input values to apply the fuzzy logic on.
    :param d: The optimal values for the input values.
    :param n: The normalization values for each of the inputs.
    :return: Relation percentage.
    """
    assert len(c) == len(d) == len(n) == 4

    # Cast everything to floats
    c = [float(v) for v in c]
    d = [float(v) for v in d]
    n = [float(v) for v in n]

    c = [c[i] / n[i] for i in range(4)]
    d = [d[i] / n[i] for i in range(4)]

    wa = 1
    wb = 1

    min_low = min(c[0], d[0])
    max_high = max(c[3], d[3])
    a = [(c[i] - min_low) / (max_high - min_low) for i in range(4)]
    b = [(d[i] - min_low) / (max_high - min_low) for i in range(4)]

    sum_abs_diff = sum([abs(a[i] - b[i]) for i in range(4)])
    s2 = 1 - sum_abs_diff / 4

    if a[0] != a[3]:
        ysa = (wa * ((a[2] - a[1]) / (a[3] - a[0]) + 2)) / 6
    else:
        ysa = wa / 2

    if wa == 0:
        xsa = (a[3] + a[0]) / 2
    else:
        xsa = (ysa * (a[2] + a[1]) + (a[3] + a[0]) * (wa - ysa)) / (2 * wa)

    if b[0] != b[3]:
        ysb = (wb * ((b[2] - b[1]) / (b[3] - b[0]) + 2)) / 6
    else:
        ysb = wb / 2

    if wb == 0:
        xsb = (b[3] + b[0]) / 2
    else:
        xsb = (ysb * (b[2] + b[1]) + (b[3] + b[0]) * (wb - ysb)) / (2 * wb)

    if a[3] != a[0]:
        pa = math.sqrt(math.pow(a[0] - a[1], 2) + wa * wa) + math.sqrt(math.pow(a[2] - a[3], 2) + wa * wa) + a[2] - a[1] + a[3] - a[0]

    if a[3] == a[0]:
        pa = wa

    if b[3] != b[0]:
        pb = math.sqrt(math.pow(b[0] - b[1], 2) + wb * wb) + math.sqrt(math.pow(b[2] - b[3], 2) + wb * wb) + b[2] - b[1] + b[3] - b[0]

    if b[3] == b[0]:
        pb = wb

    aa = 0.5 * wa * (a[1] - a[0] + a[3] - a[2]) + (a[2] - a[1]) * wa
    ab = 0.5 * wb * (b[1] - b[0] + b[3] - b[2]) + (b[2] - b[1]) * wb

    s4 = (min(pa, pb) + min(aa, ab)) / (max(pa, pb) + max(aa, ab))

    v = 1 - abs(xsa - xsb)

    z = (abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2]) + abs(a[3] - b[3])) / 4

    t = abs(xsa - xsb)

    tz = -abs(z * t)

    if c[0] == c[1] == c[2] == c[3] and d[0] == d[1] == d[2] == d[3]:
        s21 = (math.pow(math.exp(1), tz)) * s4
    else:
        try:
            s21 = math.pow(s2, v) * s4
        except ValueError:
            return 0

    return s21 if s21 > 0 else 0

