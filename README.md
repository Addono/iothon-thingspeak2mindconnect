# IoThon IoT MindConnect

```shell
# Clone the repository
git clone https://gitlab.com/Addono/iothon-thingspeak2mindconnect.git
cd iothon-thingspeak2mindconnect

# Optional but recommended to create a virtual environment to prevent 
# globally installing the dependencies
virtualenv venv
source venv/bin/activate

# Install dependencies
pip install -r requirements.txt

# Update configuration, feel free to use whatever editor suits you best
vi config.yaml

# Run the application
python main.py
```

